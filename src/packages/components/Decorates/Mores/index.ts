import { NumberConfig } from './Number/index'
import { TimeCommonConfig } from './TimeCommon/index'
import { ClockConfig } from './Clock/index'

export default [TimeCommonConfig, NumberConfig, ClockConfig]
